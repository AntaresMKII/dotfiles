# .bashrc

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '
if [ -z $DISPLAY ] && [ "$(tty)" == "/dev/tty1" ]; then
    exec sway
fi

export MOZ_ENABLE_WAYLAND=1
export PATH=/usr/local/bin:/usr/local/sbin:/usr/bin:/usr/sbin:/bin:/sbin:/lib/jvm/jdk-16.0.1/bin:/home/yannick/idea-IC-211.7142.45/bin
export _JAVA_AWT_WM_NONREPARENTING=1
