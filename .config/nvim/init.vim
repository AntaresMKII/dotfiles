call plug#begin()
	Plug 'tpope/vim-surround' " change the brakets using cs
	Plug 'scrooloose/syntastic'
	Plug 'dylanaraps/wal.vim'
call plug#end()

" Syntastic settings
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

colorscheme wal
